#Beacon Store Manager
### A simple Node.js + Express + MongoDB CRUD for iBeacons

You must have installed:

* Node.js
* Express.js
* MongoDB
* Mongoose

After cloning, you must run **npm install**
and:

$ **node bin/www** 

or 

$ **nodemon** 

on command line.

